package factoryMethod;

import factoryMethod.AbstractFactory;
import factoryMethod.Transport.*;

public class AnimalFactory extends AbstractFactory {
    @Override
    Transport getTransport(TransportType transportType) {
        // bede zwracac obiekt transport
        Transport transport;
        switch (transportType) {
            case BUS_TRUCK:
                transport = new AnimalTruck();
                break;
            case SHIP:
                transport = new AnimalShip();
                break;
            case PLANE:
                transport = new AnimalPlane();
                break;
            default:
                transport = null;
        }
        // i zwracam obiekt transport, jak nadmienilem w linii 8
        return transport;
    }
}

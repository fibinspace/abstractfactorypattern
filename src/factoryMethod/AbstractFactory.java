package factoryMethod;

import factoryMethod.Transport.Transport;
import factoryMethod.Transport.TransportType;

public abstract class AbstractFactory {
    // klasa posiada 1 metode getTransport, ktora jako argument
    // przyjmuje TransportType
    abstract Transport getTransport(TransportType transportType);
}

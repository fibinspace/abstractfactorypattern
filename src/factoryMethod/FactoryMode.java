package factoryMethod;

public enum FactoryMode {
    PASSENGER,
    CARGO,
    ANIMAL
}

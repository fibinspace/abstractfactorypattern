package factoryMethod;

import factoryMethod.Transport.*;

public class CargoFactory extends AbstractFactory{
    @Override
    Transport getTransport(TransportType transportType) {
        // bede zwracac obiekt transport
        Transport transport;
        switch (transportType) {
            case BUS_TRUCK:
                transport = new CargoTruck();
                break;
            case SHIP:
                transport = new CargoShip();
                break;
            case PLANE:
                transport = new CargoPlane();
                break;
            default:
                transport = null;
        }
        // i zwracam obiekt transport, jak nadmienilem w linii 8
        return transport;
    }
}
